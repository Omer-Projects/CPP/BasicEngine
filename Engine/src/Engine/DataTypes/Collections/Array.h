#pragma once

#include "Engine/Core.h"
#include "Engine/DataTypes/Collections/BasicArray.h"

namespace Engine::DataTypes::Collections
{
	template <typename T, const SizeT LENGTH>
	class Array : public BasicArray<T, SizeT, LENGTH>
	{

	};
}