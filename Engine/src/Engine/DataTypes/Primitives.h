#pragma once

// This is Core class

/// Not need to define:
/// bool, char, short, int, float, double
///

namespace Engine::DataTypes
{
	using Byte = unsigned char;
	using ushort = unsigned short;
	using uint = unsigned int;
	using Long = long long;
	using Ulong = unsigned long long;

	using SizeT = uint;
}