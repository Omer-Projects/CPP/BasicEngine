# Engine

# Working on


# Status
In all the system
	ToString

DataTypes::Iterators
	ReadIterator and WriteIterator not working right
DataTypes::Collections::Array
	#include <utility> exits!
	Cheaks in debug mode
DataTypes::Collections::DynamicArray
	#include <utility> exits!
	Cheaks in debug mode
	To cheak if it is working right
DataTypes::Streams
	To implemente.

# TODO

DataTypes/Collections
	Implementate Iterator class / interface
	Implementate the advence collections
	Implementate the String class
	
DataTypes/DynamicAllocators
	Implementate abstract DynamicMemoryAllocator class
	Implementate "Heap" DynamicMemoryAllocator (defualt)

DataTypes::TypeInfo
	ID

	GetID()
	GetName()
	GetFullName()
	GetBase()
	GetSize()

	operator==
	operator!=

# General

Create the <utility> functions:
	std::move
	std::forward
Remove the <utility> include

# Learning
Learn how to do const T& and t& functons with the same name
	for the ReadIterator implementation
Learn return Setter value without Getter.
	for the WriteIterator implementation
Learn how the Heap collection work.
Learn What is the collections Set and Map?

# Absolute decisions
To create -> operator for iterators? is not working for all the types
Iterators from ForwardIterator and above need WriteIterator? for the write element function
TypeInfo be in Release (the TypeId)?
Rename SizeT to LengthT?