#pragma once

#define INFO_CATEGORY 0

#include "Debug/Logger.h"
#include "Debug/Memory.h"

namespace Debug
{
	void InitSettings()
	{
		Debug::ShowMemoryInfo(false);
	}
}