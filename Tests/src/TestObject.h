#pragma once

static bool s_ShowObjectInfo = false;

class TestObject
{
public:
	int m_Value;


	TestObject()
		: m_Value(0)
	{
		if (s_ShowObjectInfo)
		{
			LOG("Object", "Create");
		}
	}

	TestObject(int value)
		: m_Value(value)
	{
		if (s_ShowObjectInfo)
		{
			LOG("Object", "Create");
		}
	}

	TestObject(const TestObject& object)
		: m_Value(object.m_Value)
	{
		if (s_ShowObjectInfo)
		{
			LOG("Object", "Copy");
		}
	}

	TestObject(TestObject&& object)
		: m_Value(object.m_Value)
	{
		if (s_ShowObjectInfo)
		{
			LOG("Object", "Move");
		}
	}

	~TestObject()
	{
		if (s_ShowObjectInfo)
		{
			LOG("Object", "Destory");
		}
	}

	TestObject& operator=(const TestObject& other)
	{
		if (s_ShowObjectInfo)
		{
			LOG("Object", "Move");
		}
		
		m_Value = other.m_Value;
		return *this;
	}
};