#include <iostream>

#include "Debug.h"
#include "Tests/Collections.h"

int main()
{
	Debug::InitSettings();


	Tests::Collections::TestArray();
	std::cout << ".........\n";
	Tests::Collections::TestDynamicArray();
	std::cout << ".........\n";
	
	Tests::Collections::TestStack();
	std::cout << ".........\n";
	Tests::Collections::TestQueue();
	std::cout << ".........\n";
	Tests::Collections::TestDeque();
	std::cout << ".........\n";
	Tests::Collections::TestList();
	std::cout << ".........\n";
	Tests::Collections::TestCircularLinkedList();
	std::cout << ".........\n";
	Tests::Collections::TestBinarySearchTree();
	std::cout << ".........\n";

	std::cout << "Press any key to continue . . .";
	std::cin.get();
}